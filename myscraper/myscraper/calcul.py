# -*- coding: utf-8 -*-
# This file is part of Aspiturf.
# Aspiturf is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
# Aspiturf is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
# You should have received a copy of the GNU General Public License along with Aspiturf.  If not, see <https://www.gnu.org/licenses/>.

from __future__ import division
from twisted.enterprise import adbapi
import MySQLdb
from scrapy.utils.project import get_project_settings
import datetime
import re
import sys
import time

settings = get_project_settings()
# reload(sys)
# sys.setdefaultencoding('utf8')

class Calcul(object):
    # c.execute("""SELECT spam, eggs, sausage FROM breakfast WHERE price < %s""", (max_price,))

    def __init__(self):
        dbargs = settings.get('DB_CONNECT')
        db_server = settings.get('DB_SERVER')
        # dbpool = adbapi.ConnectionPool(db_server, **dbargs)
        # self.dbpool = dbpool
        self.conn = MySQLdb.connect(**dbargs)
        self.cursor = self.conn.cursor()

    def Recense(self, cheval, date):
        dateFormat = datetime.datetime.strptime(date, '%Y-%m-%d').date()
        sql = """SELECT `jour` FROM `cachedate` WHERE `cheval` = %s AND `jour` < %s AND `cl` != 'NP' ORDER BY `cachedate`.`jour` DESC limit 1"""

        self.cursor.execute(sql, (cheval, date))
        m = self.cursor.fetchone()

        if (m != None):
            try:
                recense = dateFormat - m[0]
                return recense.days
            except:
                return 0
        return 0

    def MusiqueCheval(self, cheval, date):
        sql = """SELECT `jour`, `cl`, `cheval` FROM `cachedate` WHERE `cheval` = %s AND `jour` < %s ORDER BY `cachedate`.`jour` DESC """

        musiqueCheval = [0, 0, 0, 0, 0, 0]
        self.cursor.execute(sql, (cheval, date))
        musiques = self.cursor.fetchall()

        if (musiques != None):
            try:
                i = 0
                for musique in musiques:
                    if (musique[1] != None and i < 6 and musique[1] != 'NP' and musique[1] != ''):
                        musiqueCheval[i] = musique[1]
                        # print ('musiqueCheval[i]', musiqueCheval[i])
                        musiqueCheval[i] = re.sub('[0-9][0-9]e dist.$', '10', musiqueCheval[i])
                        musiqueCheval[i] = re.sub('[0-9]e dist.$', '10', musiqueCheval[i])
                        musiqueCheval[i] = re.sub('dai [0-9]e $', '10', musiqueCheval[i])
                        musiqueCheval[i] = re.sub('[0-9]e dad$', '10', musiqueCheval[i])
                        musiqueCheval[i] = re.sub('[0-9]e \(a.ram.\)$', '10', musiqueCheval[i])
                        musiqueCheval[i] = re.sub('[0-9]e \(d.ram.\)$', '10', musiqueCheval[i])
                        musiqueCheval[i] = re.sub('[0-9][0-9]e \([0-9]e r.t.\)$', '10', musiqueCheval[i])
                        musiqueCheval[i] = re.sub('[0-9][0-9]e \(1er r.t.\)$', '10', musiqueCheval[i])
                        musiqueCheval[i] = re.sub('2e \([0-9]e r.t.\)$', '2', musiqueCheval[i])
                        musiqueCheval[i] = re.sub('3e \([0-9]e r.t.\)$', '3', musiqueCheval[i])
                        musiqueCheval[i] = re.sub('4e \([0-9]e r.t.\)$', '4', musiqueCheval[i])
                        musiqueCheval[i] = re.sub('5e \([0-9]e r.t.\)$', '5', musiqueCheval[i])
                        musiqueCheval[i] = re.sub('6e \([0-9]e r.t.\)$', '6', musiqueCheval[i])
                        musiqueCheval[i] = re.sub('7e \([0-9]e r.t.\)$', '7', musiqueCheval[i])
                        musiqueCheval[i] = re.sub('8e \([0-9]e r.t.\)$', '8', musiqueCheval[i])
                        musiqueCheval[i] = re.sub('9e \([0-9]e r.t.\)$', '9', musiqueCheval[i])
                        musiqueCheval[i] = re.sub('2e \(1er r.t.\)$', '2', musiqueCheval[i])
                        musiqueCheval[i] = re.sub('3e \(1er r.t.\)$', '3', musiqueCheval[i])
                        musiqueCheval[i] = re.sub('4e \(1er r.t.\)$', '4', musiqueCheval[i])
                        musiqueCheval[i] = re.sub('5e \(1er r.t.\)$', '5', musiqueCheval[i])
                        musiqueCheval[i] = re.sub('6e \(1er r.t.\)$', '6', musiqueCheval[i])
                        musiqueCheval[i] = re.sub('7e \(1er r.t.\)$', '7', musiqueCheval[i])
                        musiqueCheval[i] = re.sub('8e \(1er r.t.\)$', '8', musiqueCheval[i])
                        musiqueCheval[i] = re.sub('9e \(1er r.t.\)$', '9', musiqueCheval[i])
                        musiqueCheval[i] = re.sub('[0-9][0-9]e \(t.rem\)$', '10', musiqueCheval[i])
                        musiqueCheval[i] = re.sub('[0-9][0-9]e tbé$', '10', musiqueCheval[i])
                        musiqueCheval[i] = re.sub('[0-9]e dpg$', '10', musiqueCheval[i])
                        musiqueCheval[i] = re.sub('[0-9]e disq.$', '10', musiqueCheval[i])
                        # musiqueCheval[i] = musiqueCheval[i].replace('2e (1er rét.)', '2')
                        # musiqueCheval[i] = musiqueCheval[i].replace('4e (3e rét.)', '4')
                        # musiqueCheval[i] = musiqueCheval[i].replace('5e (2e rét.)', '5')
                        musiqueCheval[i] = musiqueCheval[i].replace('1er dist.', '10')
                        musiqueCheval[i] = musiqueCheval[i].replace('1er disq.', '10')
                        musiqueCheval[i] = musiqueCheval[i].replace('1er dpg', '10')
                        musiqueCheval[i] = musiqueCheval[i].replace('dist.', '10')
                        musiqueCheval[i] = musiqueCheval[i].replace('disq.', '10')
                        musiqueCheval[i] = musiqueCheval[i].replace('dai 1er ', '10')
                        musiqueCheval[i] = musiqueCheval[i].replace('dai 12e (', '10')
                        musiqueCheval[i] = musiqueCheval[i].replace('tbée', '10')
                        musiqueCheval[i] = musiqueCheval[i].replace('tbé a.', '10')
                        musiqueCheval[i] = musiqueCheval[i].replace('tbé', '10')
                        musiqueCheval[i] = musiqueCheval[i].replace('rét.', '10')
                        musiqueCheval[i] = musiqueCheval[i].replace('rp', '10')
                        musiqueCheval[i] = musiqueCheval[i].replace('dai', '10')
                        musiqueCheval[i] = musiqueCheval[i].replace('dad', '10')
                        musiqueCheval[i] = musiqueCheval[i].replace('dpg', '10')
                        musiqueCheval[i] = musiqueCheval[i].replace('Npl.', '10')
                        # musiqueCheval[i] = musiqueCheval[i].replace('NP', '10')
                        musiqueCheval[i] = musiqueCheval[i].replace('dér.t.', '10')
                        musiqueCheval[i] = musiqueCheval[i].replace('dér.a.', '10')
                        musiqueCheval[i] = musiqueCheval[i].replace('d.ram.', '10')
                        musiqueCheval[i] = musiqueCheval[i].replace('a.ram.', '10')
                        musiqueCheval[i] = musiqueCheval[i].replace('dér.', '10')
                        musiqueCheval[i] = musiqueCheval[i].replace('arr.', '10')
                        musiqueCheval[i] = musiqueCheval[i].replace('er', '')
                        musiqueCheval[i] = musiqueCheval[i].replace('e', '')
                        musiqueCheval[i] = int(musiqueCheval[i])
                        i += 1
            except Exception as e:
                return musiqueCheval
        return musiqueCheval

    def MusiqueHistory(self, titlename, name, date):
        dateFormat = datetime.datetime.strptime(date, '%Y-%m-%d').date()
        dateFormatMinusYear = dateFormat - datetime.timedelta(days=365)
        dateFormatMinusYear = dateFormatMinusYear.strftime('%Y-%m-%d')

        sql = """SELECT `jour`, `cl`, `""" + titlename + """`, `typec` FROM `cachedate` WHERE `""" + titlename + """` = %s AND `jour` < %s"""
        if (titlename != 'cheval'):
            sql += """   AND `jour` >= %s """
        # if (titlename != 'cheval'):
        #     sql += """ limit 30"""
        sql += """  AND `cl` != 'NP' ORDER BY `cachedate`.`jour` DESC """
        
        # if (titlename != 'cheval'):
        #     sql += """ limit 1,100 """

        # [Nbcourses, musique, courses gagnées, courses placées]
        musiqueName = [0,u'',0,0]
        if (name == "0"):
            # print('name', name)
            return musiqueName
        if (titlename != 'cheval'):
            self.cursor.execute(sql, (name, date, dateFormatMinusYear))
        else:
            self.cursor.execute(sql, (name, date))
        musiques = self.cursor.fetchall()
        musiqueName[0] = int(self.cursor.rowcount)

        # print('name', name)

        yearPresent = str(dateFormat.year)[-2:]
        if (musiques != None):
            # try:
            countMusique = 0
            if (titlename == 'cheval'):
                maxMusique = 10000
            else:
                maxMusique = 100

            for musique in musiques:
                # if (titlename == 'cheval'):
                    # print('musique', musique)
                year = str(musique[0].year)[-2:]

                if (musique[1] != None and musique[1] != 'NP' and musique[1] != ''):
                    if (countMusique < maxMusique):
                        if (year != yearPresent):
                            musiqueName[1] += '(' + year + ') '
                            yearPresent = year
                        if (titlename == 'cheval' and musique[1] != ''):
                            musiqueName[1] += musique[1]
                        if (titlename != 'cheval'):
                            musiqueName[1] += musique[1]
                        # if (titlename == 'cheval'):
                            # print('musiqueName',musiqueName[1])
                        musiqueName[1] = musiqueName[1].replace('dai 1er ', 'D')
                        musiqueName[1] = musiqueName[1].replace('dai 4e ', 'D')
                        musiqueName[1] = musiqueName[1].replace('dai 5e ', 'D')
                        musiqueName[1] = musiqueName[1].replace('dai 3e ', 'D')
                        musiqueName[1] = musiqueName[1].replace('dai 2e ', 'D')
                        musiqueName[1] = musiqueName[1].replace('dai 6e ', 'D')
                        musiqueName[1] = musiqueName[1].replace('dai 8e ', 'D')
                        musiqueName[1] = musiqueName[1].replace('dai 7e ', 'D')
                        musiqueName[1] = musiqueName[1].replace('dai 9e ', 'D') 
                        musiqueName[1] = musiqueName[1].replace('er', '')
                        musiqueName[1] = musiqueName[1].replace('e', '')
                        musiqueName[1] = musiqueName[1].replace('dai', 'D')
                        musiqueName[1] = musiqueName[1].replace('tbé', 'T')
                        musiqueName[1] = musiqueName[1].replace('dér.t.', 'Dt')
                        musiqueName[1] = musiqueName[1].replace('arr.', 'A')
                        # if (titlename == 'cheval'):
                            # print('musiqueName',musiqueName[1])
                        try:
                            musiqueName[1] += str(musique[3])[0].lower() + ' '
                        except:
                            musiqueName[1] += ' '
                    if (musique[1] == '1er'):
                        musiqueName[2] += 1
                    if (musique[1] == '1er' or musique[1] == '2e' or musique[1] == '3e'):
                        musiqueName[3] += 1
                    countMusique += 1
            # except Exception as e:
                # return musiqueName
        return musiqueName

    def MonteJour(self, titlename, name, date):
        sql = """SELECT COUNT(id) as nb FROM `cachedate` WHERE `""" + titlename + """` = %s AND `jour` = %s """
        return self.CleanResult(sql, name, date)

    def CouruJour(self, titlename, name, date):
        sql = """SELECT COUNT(id) as nb FROM `cachedate` WHERE `""" + titlename + """` = %s AND `jour` = %s and `cl` != '' """
        return self.CleanResult(sql, name, date)

    def VictoireJour(self, titlename, name, date):
        sql = """SELECT COUNT(id) as nb FROM `cachedate` WHERE `""" + titlename + """` = %s AND `jour` = %s and `cl` = '1er' """
        return self.CleanResult(sql, name, date)

    def CleanResult(self, sql, name, date):
        traiteVar = ''
        self.cursor.execute(sql, (name, date))
        traiteVar = self.cursor.fetchone()

        traiteVar = str(traiteVar[0])

        # print(name, 'traiteVar=============>', traiteVar)
        return traiteVar

    def Dernier(self, cheval, date):
        # print('calcul Dernier=============', date)
        sql = """SELECT `numcourse`, `ch`.`jour`, `cl`, `cheval`, `jockey`, `entraineur`, `proprietaire`, `coteprob`, `cr`.`dist`, `cr`.`hippo`, `cr`.`partant`, `cr`.`cheque`, `ch`.`defoeil`, `ch`.`redkm`, `ch`.`txreclam` FROM `cachedate` AS ch LEFT JOIN caractrap AS cr on cr.id = ch.numcourse WHERE `cheval` = %s AND `ch`.`jour` < %s ORDER BY `ch`.`jour` DESC LIMIT 1"""
        self.cursor.execute(sql, (cheval, date))
        result = self.cursor.fetchone()
        if (result == None):
            result = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
        return result

        # `numcourse`, `ch`.`jour`, `cl`, `cheval`, `jockey`, `entraineur`, `proprietaire`, `coteprob`, `cr`.`dist`, `cr`.`hippo`, `cr`.`partant`, `cr`.`cheque`, , `cr`.`defoeil`

    def PourcVictHippo(self, titlename, name, hippo, date):
        # print('calcul PourcVictHippo=============', date)
        results = 0
        resultGagnant = 0
        resultPlace = 0
        pourc = [0, 0, 0]
        sql = """SELECT `numcourse`, `jour`, `cl`, `cheval`, `jockey`, `entraineur`, `hippo` FROM `cachedate` WHERE `""" + titlename + """` = %s AND `hippo` = %s AND `jour` < %s  """
        self.cursor.execute(sql, (name, hippo, date))
        results = self.cursor.fetchall()
        nbTotal = int(self.cursor.rowcount)
        for result in results:
            if (result[2] == '1er'):
                # print('resultGagnant', results)
                # print('1er ===============>', result[2])
                resultGagnant += 1
            if (result[2] == '1er' or result[2] == '2e' or result[2] == '3e'):
                resultPlace += 1
        # pourc[0] => gagnant
        # pourc[1] => place
        # pourc[2] => nombre de course sur cet hippo
        try:
            pourc[0] = resultGagnant / nbTotal * 100
        except ZeroDivisionError:
            pourc[0] = 0
        try:
            pourc[1] = resultPlace / nbTotal * 100
        except ZeroDivisionError:
            pourc[1] = 0
        pourc[2] = nbTotal
        return pourc

    def couple(self, jockey, cheval, hippo, date):
        # print('calcul couple=============', date)
        results = 0
        resultGagnant = 0
        resultPlace = 0
        pourc = [0, 0, 0, 0, 0]
        sql = """SELECT `numcourse`, `jour`, `cl`, `cheval`, `jockey` FROM `cachedate` WHERE `jockey` = %s AND `cheval` = %s AND `hippo` REGEXP %s AND `jour` < %s  """
        self.cursor.execute(sql, (jockey, cheval, hippo, date))
        results = self.cursor.fetchall()
        nbTotal = int(self.cursor.rowcount)
        for result in results:
            if (result[2] == '1er'):
                resultGagnant += 1
            if (result[2] == '1er' or result[2] == '2e' or result[2] == '3e'):
                    resultPlace += 1
        # pourc[0] => gagnant
        # pourc[1] => place
        # pourc[2] => nombre de course sur cet hippo
        # pourc[3] => %gagnant
        # pourc[4] => %place
        pourc[0] = resultGagnant
        pourc[1] = resultPlace
        pourc[2] = nbTotal
        try:
            pourc[3] = resultGagnant / nbTotal * 100
        except ZeroDivisionError:
            pourc[3] = 0
        try:
            pourc[4] = resultPlace / nbTotal * 100
        except ZeroDivisionError:
            pourc[4] = 0
        return pourc

    def RecordG(self, cheval, date):
        # print('calcul RecordG=============', date)
        result = ''
        sql = """SELECT `redkm` FROM `cachedate` WHERE `cheval` = %s AND `redkm` != "0" AND `jour` < %s ORDER BY `cachedate`.`redkm` ASC limit 1  """
        self.cursor.execute(sql, (cheval, date,))
        # results = self.cursor.fetchall()
        result = self.cursor.fetchone()
        # print('RecordG', result)
        return result

    def Recul(self, date):
        # print('calcul Recul=============', date)
        sql = """UPDATE cachedate AS t INNER JOIN (SELECT comp, min(dist) mdist FROM cachedate WHERE `jour` = %s GROUP BY comp) t1 ON t.comp = t1.comp AND t.dist > t1.mdist  SET recul = 1  """
        self.cursor.execute(sql, (date,))
        self.conn.commit()

    def AppetenceMeteo(self, cheval, date):
        """
        Return 0 si cheval jamais couru sur ce type de terrain
        Return 1 s'il a déjà fait dans les 4 premiers
        Return 2 s'il a déjà couru mais n'est pas arrivé dans les 4 premiers
        type de terrain défini par le champ météo (lourd, léger, dur, bon...)
        """
        result = 0

        sql = """ SELECT `meteo` FROM caractrap \
            left join `cachedate` on `caractrap`.`id` = `cachedate`.`numcourse` \
            WHERE `cheval` = %s \
            AND `caractrap`.`jour` = %s \
            """
        self.cursor.execute(sql, (cheval, date))
        meteo = self.cursor.fetchone()

        if (meteo != None and meteo[0] != '' and meteo[0] != 'Terrain '):
            sql = """SELECT `cl`  FROM caractrap \
                left join `cachedate` on `caractrap`.`id` = `cachedate`.`numcourse` \
                WHERE `cheval` = %s \
                AND `caractrap`.`jour` < %s \
                AND `caractrap`.`meteo` = %s 
                """
            self.cursor.execute(sql, (cheval, date, meteo))
            results = self.cursor.fetchall()
            result = int(self.cursor.rowcount)
            if result > 0:
                # cheval à déjà couru sur ce type de terrain
                result = 2

                for item in results:
                    # cheval à déjà fini dans les 4 premiers sur ce type de terrain
                    if "1er" in item or "2e" in item or "3e" in item or "4e" in item:
                        # print('item', item)
                        result = 1


            # print('result', result)
            # print('results', results)
            # print('-/-/-/-/-/-/')
        return result

    def AppetenceTerrain(self, cheval, date):
        """ 
        Return 0 si cheval jamais couru sur ce type de piste
        Return 1 s'il a déjà fait dans les 4 premiers
        Return 2 s'il a déjà couru mais n'est pas arrivé dans les 4 premiers
        type de piste défini par la table hippo (herbe, dur, pouzzolane, sable...)
        """
        result = 0

        # Cherche type course et hippo
        sql = """ SELECT  `typec`, `hippo` FROM cachedate \
            WHERE `cheval` = %s \
            AND `jour` = %s
            """
        self.cursor.execute(sql, (cheval, date))
        typec = self.cursor.fetchone()
        
        if (typec == None or typec[0] == 0 ):
            return 10

        hippoName = typec[1]
        type = self.typeTerrain(typec)
        # print(typec)
        # print(type)

        # Cherche numéro de type de piste
        sql = """ SELECT  `hippo`, """ + type + """ FROM hippo \
            WHERE `hippo` = %s 
            """
        self.cursor.execute(sql, (hippoName,))
        hippo = self.cursor.fetchone()
        if (hippo == None ):
            return 10
        typeTerrain = hippo[1]
        # print('typeTerrain', typeTerrain)

        # Cherche si cheval a déjà couru sur ce type de terrain
        sql = """ SELECT `cl` FROM cachedate  \
            left join `hippo` on `cachedate`.`hippo` = `hippo`.`hippo`  \
            WHERE `cheval` = %s \
            AND `cachedate`.`jour` < %s  \
            AND `hippo`.""" + type + """ = %s
            """
        self.cursor.execute(sql, (cheval, date, typeTerrain))
        # print(self.cursor._last_executed)
        results = self.cursor.fetchall()
        # print('results', results)
        result = int(self.cursor.rowcount)
        if result > 0:
            # cheval à déjà couru sur ce type de terrain
            result = 2

            for item in results:
                # cheval à déjà fini dans les 4 premiers sur ce type de terrain
                if "1er" in item or "2e" in item or "3e" in item or "4e" in item:
                    # print('item', item)
                    result = 1

        # print('result', result)
        return result
        

    def typeTerrain(self, typec):
        if typec[0] in ('Attel' , 'Attelé', 'Mont', 'Monté'):
            return 'trot'
        if typec[0] in ('Haies' , 'Steeple-chase', 'Steeple-chase cross-country'):
            return 'obstacle'
        if typec[0] == 'Plat':
            return 'plat'


    def CalculPourc(self, date):

        sql = """UPDATE cachedate SET pourcVictJock = `ROUND`(`victoiresjockey`*100/`coursesjockey`) WHERE `jour` = %s  """
        self.cursor.execute(sql, (date,))
        self.conn.commit()
        sql = """UPDATE cachedate SET pourcPlaceJock = `ROUND`(`placejockey`*100/`coursesjockey`) WHERE `jour` = %s  """
        self.cursor.execute(sql, (date,))
        self.conn.commit()

        sql = """UPDATE cachedate SET pourcVictCheval = `ROUND`(`victoirescheval`*100/`coursescheval`) WHERE `jour` = %s  """
        self.cursor.execute(sql, (date,))
        self.conn.commit()
        sql = """UPDATE cachedate SET pourcPlaceCheval = `ROUND`(`placescheval`*100/`coursescheval`) WHERE `jour` = %s  """
        self.cursor.execute(sql, (date,))
        self.conn.commit()

        sql = """UPDATE cachedate SET pourcVictEnt = `ROUND`(`victoiresentraineur`*100/`coursesentraineur`) WHERE `jour` = %s  """
        self.cursor.execute(sql, (date,))
        self.conn.commit()

        sql = """UPDATE cachedate SET pourcPlaceEnt = `ROUND`(`placeentraineur`*100/`coursesentraineur`) WHERE `jour` = %s  """
        self.cursor.execute(sql, (date,))
        self.conn.commit()

        #Met à jour indicateurInedit si recence = 0
        sql = """UPDATE cachedate SET `indicateurInedit` = 1 WHERE `recence` = 0 AND `jour` = %s  """
        self.cursor.execute(sql, (date,))
        self.conn.commit()


        sql = """UPDATE cachedate SET `indicateurInedit` = 0 WHERE `recence` != 0 AND `jour` = %s  """
        self.cursor.execute(sql, (date,))
        self.conn.commit()



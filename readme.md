# Aspiturf

Extraire les données de sites de turf afin de les utiliser dans une base de données

## Pré-requis

* Python = 3.5+

* Dépendances

```sudo apt-get install python-pip python-dev libmysqlclient-dev```

```pip install pathlib pytz requests bs4 mysqlclient```

* [Scrapy] - Framework Python pour extraire les données d'un site web

Ubuntu 14.04 et +

```sudo apt-get install libxml2-dev libxslt1-dev zlib1g-dev libffi-dev libssl-dev```

```pip install scrapy```

* Base de données Mysql

Pour la base de données Mysql, il faut vider la variable ```sql_mode``` 

Dans PhpMyAdmin par exemple, aller dans le menu Variables et chercher sql_mode. cliquez sur Modifier et vider le contenu

## Configuration

* Créer la base de données en utilisant le fichier pturf1.sql
  * caractrap => contient une ligne par courses
  * cachedate => contient une ligne par cheval. La colonne numcourse correspond à la colonne id de la table caractrap
* Renommer le fichier ```settings.example``` => ```settings.py```
* Configurer l'accès à la base de données dans ```settings.py```

```sh
DB_CONNECT = {
    'db': 'pturf1',
    'user': 'user',
    'passwd': 'user',
    'host': 'localhost',
    'charset': 'utf8',
    'use_unicode': True,
}
```

## Utilisation

### Insérer les courses dans la base de données

Se placer dans le dossier ```/myscraper/myscraper``` (Pas toujours nécessaire suivant l'installation de scrapy)

Traite les courses de la veille, du jour et du lendamain

```scrapy crawl scraper_partant```

Si plusieurs versions de Python sont installées, on peut le lancer avec

```python3.6 - m scrapy crawl scraper_partant```

Traite les courses à une date donnée

```scrapy crawl scraper_partant -a tagdate='2019-05-01' -a i=1```

Traite les courses de plusieurs jours (à partir du 2019-05-01 + 7 jours dans l'exemple)

```scrapy crawl scraper_partant -a tagdate='2019-05-01' -a i=7```

# Remerciements

Si vous souhaitez remercier l'auteur, faite un don pour un projet libre tel que [VLC] ou [Wikimedia]

# Licence GPLv3

 > This file is part of Aspiturf.

 > Aspiturf is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

 > Aspiturf is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

 > You should have received a copy of the GNU General Public License along with Aspiturf.  If not, see <https://www.gnu.org/licenses/>.

[Scrapy]: <https://scrapy.org/>
[VLC]: <https://www.videolan.org/contribute.html#money>
[Wikimedia]: <https://dons.wikimedia.fr/souteneznous/1~mon-don>